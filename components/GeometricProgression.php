<?php

namespace app\components;

/**
 *
 */
class GeometricProgression
{
    const PROGRESSION_MIN_SIZE = 1;
    const PROGRESSION_CONTINUE_COUNT = 3;

    const PROGRESSION_TYPE_INCREMENTAL = 1;
    const PROGRESSION_TYPE_DECREASING = 2;
    const PROGRESSION_TYPE_ALTERNATING = 3;

    public $numbers;

    public $type;
    public $denominator;

    /**
     * @return array
     */
    public static function getTypeNames()
    {
        return [
            self::PROGRESSION_TYPE_INCREMENTAL => 'Возрастающая',
            self::PROGRESSION_TYPE_DECREASING => 'Убывающая',
            self::PROGRESSION_TYPE_ALTERNATING => 'Чередующаяся',
        ];
    }

    /**
     * @return bool
     */
    public function checkProgressionType()
    {
        $numbers = $this->numbers;
        $countNumbers = sizeof($numbers);
        $denominator = $numbers[1] / $numbers[0];
        for ($i = 1; $i < $countNumbers; $i++) {
            $firstNumber = $numbers[$i - 1];
            $secondNumber = $numbers[$i];
            if (($secondNumber / ($firstNumber)) != $denominator) {
                return false;
            }
        }

        if ($denominator > 1) {
            $this->type = self::PROGRESSION_TYPE_INCREMENTAL;
        } elseif ($denominator < 0) {
            $this->type = self::PROGRESSION_TYPE_ALTERNATING;
        } else {
            $this->type = self::PROGRESSION_TYPE_DECREASING;
        }
        $this->denominator = $denominator;

        return true;
    }

    /**
     * @param $countNumbers
     * @return array
     */
    public function getContinuedGeometricProgression(int $countNumbers = self::PROGRESSION_CONTINUE_COUNT)
    {
        $numbers = $this->numbers;
        if($this->type === GeometricProgression::PROGRESSION_TYPE_DECREASING){
            for ($i = 0; $i < $countNumbers; $i++) {
                $lastNumber = end($numbers);
                $numbers[] = $lastNumber / $this->denominator;
            }
        }else{
            for ($i = 0; $i < $countNumbers; $i++) {
                $lastNumber = end($numbers);
                $numbers[] = $lastNumber * $this->denominator;
            }
        }
        return $numbers;
    }
}