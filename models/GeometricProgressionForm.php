<?php

namespace app\models;

use app\components\GeometricProgression;
use yii\base\Model;

/**
 *
 */
class GeometricProgressionForm extends Model
{
    public $numbers;
    /**
     * @var $progression GeometricProgression
     */
    private $progression = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['numbers', 'each', 'rule' => ['double']],
            ['numbers', 'validateProgression'],
        ];
    }

    /**
     * @param $attribute
     * @return void
     */
    public function validateProgression($attribute)
    {
        if (!$this->hasErrors()) {
            if (!is_array($this->numbers) || count($this->numbers) <= GeometricProgression::PROGRESSION_MIN_SIZE) {
                $this->addError($attribute, 'Минимальное количество чисел для прогрессии 2.');
                return;
            }

            $progression = $this->getProgression();

            if ($progression->checkProgressionType()) {
                return;
            }

            $this->addError($attribute, 'Набор чисел не является прогрессией.');
        }
    }

    /**
     *
     * @return GeometricProgression|bool
     */
    public function getProgression()
    {
        if ($this->progression === false) {
            $this->progression = new GeometricProgression();
            $this->progression->numbers = $this->numbers;
        }

        return $this->progression;
    }

    /**
     * @return string
     */
    public function getResultMessage()
    {
        $numbers = implode(', ', $this->progression->getContinuedGeometricProgression());

        return "Тип прогрессии: " . GeometricProgression::getTypeNames()[$this->progression->type]
            . "\nЗнаменатель: " . $this->progression->denominator
            . "\nПродолжение прогрессии: " . $numbers;
    }
}