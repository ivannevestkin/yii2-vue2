import router from './routes.js'
import styles from './styles/main.scss'

require('./bootstrap');

window.Vue = require('vue');

window.app = new Vue({
    el: '#app',
    router,
    styles,
    data: {

    },
    methods: {
        isActiveMenu(path) {
            return window.location.pathname == path;
        }
    },
});
