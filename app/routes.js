import VueRouter from 'vue-router'

let MainPage = require('./pages/MainPage.vue');

let routes = [
    { path: '/', component: MainPage, name: 'main'},
];

let router = new VueRouter({
    mode: 'history',
    routes
});

export default router;
