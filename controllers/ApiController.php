<?php

namespace app\controllers;

use app\models\GeometricProgressionForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class ApiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'login' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeAction($action)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        $result['token'] = \Yii::$app->request->csrfToken;
        return $result;
    }

    /**
     * Progression action.
     *
     * @return array
     */
    public function actionProgression()
    {
        $model = new GeometricProgressionForm();
        $model->load(Yii::$app->request->post(), '');
        $model->numbers = ArrayHelper::getColumn($model->numbers, 'value');

        if ($model->validate()) {
            return ['result' => 'success', 'messages' => $model->getResultMessage()];
        } else {
            return ['result' => 'error', 'messages' => $model->getFirstErrors()];
        }
    }
}